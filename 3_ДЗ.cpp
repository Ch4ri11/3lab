﻿// 3 ДЗ.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//
#include <iostream>
#include <fstream>
using namespace std;

struct Figure
{
	Figure(string fill)
	{
		m_fill = fill;
	}
	string m_fill = "black";
};



struct Path : Figure {
	Path(int x1, int y1, int x2, int y2, int x3, int y3, int x4, int y4, string fill) : x1(x1), y1(y1), x2(x2), y2(y2), x3(x3), y3(y3), x4(x4), y4(y4), Figure(fill)
	{}
	int x1 = 0;
	int y1 = 0;
	int x2 = 0;
	int y2 = 0;
	int x3 = 0;
	int y3 = 0;
	int x4 = 0;
	int y4 = 0;
};

struct Rect : Figure {
	Rect(int x, int y , int width, int height, string fill) : x(x),y(y),width(width),height(height), Figure(fill)
	{}
	int x = 0;
	int y = 0;
	int width = 0;
	int height = 0;
};

ostream& operator << (ostream& os, const Path& p)
{
	return os << "<path d=" << '"' << "M" << p.x1 << " " << p.y1 << " " << "L" << " " << p.x2 << " " << p.y2 << " " << "L" << " " << p.x3 << " " << p.y3 << " " << "L" << " " << p.x4 << " " << p.y4 << '"' << " fill =" << '"' << p.m_fill << '"' << "/>\n";
}
ostream& operator << (ostream& os, const Rect& r)
{
	return os << "<rect x=" << '"'<< r.x << '"' << "  y=" << '"' << r.y << '"' << " width=" << '"' << r.width << '"' << " height=" << '"' << r.height <<'"' << " fill =" << '"' << r.m_fill << '"' << "/>\n";
}

int main()
{
	ofstream file;
	file.open("SVG_PICTURE.svg");
	file << "<svg xmlns= " << '"' <<"http://www.w3.org/2000/svg" << '"' << " width=" << '"' << "500" << '"' << " height= " << '"' << "500" << '"' << " viewBox= " << '"' << "0 0 500 500" << '"' << " fill= " << '"' << "none" << '"' << ">\n";
	Rect r1(40, 240, 420, 50, "#black");
	Path p1(150, 390, 310, 0, 320, 5, 160, 395, "#9D2F15");
	Path p2(180, 220, 265, 30, 440, 110, 350, 310, "#041B58");
	Rect r2(350, 330, 140, 25, "#DAA42A");
	Path p4(80, 170, 145, 0, 200, 25, 135, 195, "#467B43");
	file << r1 << '\n';
	file << p1 << '\n';
	file << p2 << '\n';
	file << r2 << '\n';
	file << p4 << '\n';
	file << "</svg>";
	return 0;
}

